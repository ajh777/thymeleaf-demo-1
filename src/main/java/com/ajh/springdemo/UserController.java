package com.ajh.springdemo;

import java.util.Arrays;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(path="/user")
public class UserController {
	private static Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserRepository userRepo;

	@GetMapping(path="/all")
	public String allUsers(Model model) {
		logger.info("HTTP GET - allUsers");
		model.addAttribute("users", userRepo.findAll());
		return "users";
	}

	@GetMapping(path="/{id}")
	public String findUserById(@PathVariable Long id, Model model) throws Exception {
		Optional<User> user = userRepo.findById(id);

		if (!user.isPresent()) {
			throw new UserNotFoundException(id);
		}

		model.addAttribute("users", Arrays.asList(user.get()));
		return "users";
	}

	@ExceptionHandler(UserNotFoundException.class)
	public ModelAndView handleCustomerNotFoundException(UserNotFoundException ex)
	{
		logger.debug("Caught exception: " + ex);
		ModelAndView model = new ModelAndView("error/404");
		model.addObject("exception", ex.toString());
		return model;
	}
}
