package com.ajh.springdemo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity // Mark as JPA entity
public class User {
	@Id // Designate it as the property that will uniquely identify the entity in the database
	@GeneratedValue(strategy=GenerationType.AUTO) // Rely on the DB to automatically generate the ID value
	private Long id;

	@NotNull
	@Size(min=3, max=50, message="Name size should between 3~50")
	private String name;

	@NotNull
	@Email(message="Invalid email")
	private String email;

	@NotNull
	@Size(min=8, max=50, message="Password size should between 8~50")
	private String password;

	public User() {} // Default constructor is required by JPA

	public User(String name, String email, String password) {
		this.name = name;
		this.email = email;
		this.password = password;
	}

	@Override
	public String toString() {
		return "User{id=" + id + ", name=" + name + ", email=" + email + ", password=" + password + "}";
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
