package com.ajh.springdemo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Thymeleaf1Application {

	public static void main(String[] args) {
		SpringApplication.run(Thymeleaf1Application.class, args);
	}

	@Bean
	public CommandLineRunner dataLoader(UserRepository userRepo) {
		return new CommandLineRunner() {

			@Override
			public void run(String... args) throws Exception {
				userRepo.save(new User("andy", "andy.h.jiang@gmail.com", "12345678"));
				userRepo.save(new User("hjiang", "hjiang@163.com", "12345678"));
			}
		};
	}
}

