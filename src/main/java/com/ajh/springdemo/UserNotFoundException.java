package com.ajh.springdemo;

public class UserNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserNotFoundException(Long id) {
		super("User with id " + id + " not  found!");
	}
}
